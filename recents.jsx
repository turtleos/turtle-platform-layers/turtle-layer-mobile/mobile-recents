import React from 'react';
import { useSelector } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';

function AppIndicator({appid, am, key, onClose=()=>{}}) {


    let app = am.am.getAppById(appid);
    return (
        <div key={key} onClick={()=>{
            onClose();
            //left/single click
            app.dispatch(new Event('focus'));
            if(am.hidden[appid]) am.am.toggleHideApp(appid);
        }} onContextMenu={(e)=>{
            //right/long click
            e.preventDefault();
            am.am.closeApp(appid);
        }} style={{
            margin:'0.4rem',
            padding:'4rem',
            borderRadius:'25px',
            backgroundImage:'linear-gradient(20deg, red, blue)',
            width:'min-content',
            height:'min-content',
            color:'white',
            flex:0.25
                  }}>
          <img onContextMenu={(e)=>{
              e.preventDefault();
          }} alt={app.name} src={app.icon} style={{
              padding:'0.4rem',
              width:'3rem',
          }}/>
          <p>
            {app.name}
          </p>
        </div>
           );
}
function MobileRecents({shown, onClose}) {
    const am = useSelector(state => state.am);
    let running = [];
    for(let win of Object.keys(am.windows)) {
        running.push(<AppIndicator onClose={onClose} am={am} key={win} appid={win}/>);
    }
    return (
        <div style={{
            height:'calc(100%)',
            top:(shown)?0:'101%',
            transition:'top .4s',
            left:0,
            width:'100%',
            background:'rgba(0,0,0,0.4)',
            position:'absolute'
        }}>
          <Scrollbars>
            <div style={{
                height:'calc(100% - 0.8rem)',
                width:'calc(100% - 0.8rem)',
                padding:'0.4rem',
                display:'flex',
                flexDirection:'row',
                flexWrap:'wrap',
                alignItems:'center',
                justifyContent:'space-evenly'
            }}>
        {running}
        </div>
        </Scrollbars>
        </div>
    );
}
export {
    MobileRecents
}
